package com.example.axce.papb

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

class ViewPagerAdapter(fm: FragmentManager?) : FragmentStatePagerAdapter(fm) {
    override fun getItem(position: Int): Fragment? {
        when(position){
            0 -> return ProfileFragment()
            1 -> return SettingFragment()
            else -> return null
        }
    }

    override fun getCount(): Int {
        return 2
    }

}