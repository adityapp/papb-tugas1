package com.example.axce.papb

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        view_pager.adapter = ViewPagerAdapter(supportFragmentManager)
        tab_layout.setupWithViewPager(view_pager)
        tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                when (tab?.position) {
                    1 -> {
                        tab_layout.setBackgroundColor(Color.parseColor("#000000"))
                        linear_layout.setBackgroundColor(Color.parseColor("#e2e2e2"))
                    }
                    else -> {
                        tab_layout.setBackgroundColor(Color.parseColor("#99000000"))
                        linear_layout.setBackgroundResource(R.drawable.background)
                    }
                }
            }

        })
        tab_layout.getTabAt(0)?.text = "Profile"
        tab_layout.getTabAt(0)?.setIcon(R.drawable.ic_person_24dp)
        tab_layout.getTabAt(1)?.text = "Setting"
        tab_layout.getTabAt(1)?.setIcon(R.drawable.ic_settings_24dp)
    }

}

